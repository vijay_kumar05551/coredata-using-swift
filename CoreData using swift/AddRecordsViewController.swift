//
//  AddRecordsViewController.swift
//  CoreData using swift
//
//  Created by OSX on 30/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

import UIKit
import CoreData

class AddRecordsViewController: UIViewController {

    @IBOutlet weak var rollNumberTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var departmentTF: UITextField!
    var manageObject = [NSManagedObject]()
    var isUpdate:Bool = false
    var index:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let saveBtn:UIButton = UIButton()
        saveBtn.frame = CGRectMake(0, 0, 40, 40)
        saveBtn.setTitle("Save", forState: UIControlState.Normal)
        saveBtn.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        saveBtn.addTarget(self, action: #selector(AddRecordsViewController.saveBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        let rightSideBarBtnItem:UIBarButtonItem = UIBarButtonItem(customView: saveBtn)
        self.navigationItem.rightBarButtonItem = rightSideBarBtnItem
        
        let cancelBtn:UIButton = UIButton()
        cancelBtn.frame = CGRectMake(0, 0, 60, 40)
        cancelBtn.setTitleColor(UIColor.blueColor(), forState: UIControlState.Normal)
        cancelBtn.setTitle("Cancel", forState: UIControlState.Normal);
        cancelBtn.addTarget(self, action: #selector(AddRecordsViewController.cancelBtnAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        let leftSideBarBtnItem:UIBarButtonItem = UIBarButtonItem(customView: cancelBtn)
        self.navigationItem.leftBarButtonItem = leftSideBarBtnItem

        if isUpdate {
            let objectModel = manageObject[index] as NSManagedObject
            rollNumberTF.text = objectModel.valueForKey("rollNumber") as? String
            nameTF.text = objectModel.valueForKey("name") as? String
            departmentTF.text = objectModel.valueForKey("department") as? String
        }
            
        else {
            
        }
    }
    
    func saveBtnAction(sender:AnyObject) {
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = delegate.managedObjectContext
        
        if isUpdate {
            let objectModel = manageObject[index] as NSManagedObject
            let num:Int = Int(rollNumberTF.text!)!
            objectModel.setValue(num, forKey: "rollNumber")
            objectModel.setValue(nameTF.text! as String, forKey: "name")
            objectModel.setValue(departmentTF.text! as String, forKey: "department")
            
            do {
                try context.save()
                
                let controller = UIAlertController(title: "", message: "Saved", preferredStyle: UIAlertControllerStyle.Alert)
                let okBtnAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (nil) in
                    self.rollNumberTF.text = ""
                    self.nameTF.text = ""
                    self.departmentTF.text = ""
                })
                
                controller.addAction(okBtnAction)
                self.presentViewController(controller, animated: true, completion: nil);
                
                //5
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
        else {
            let insertEntity = NSEntityDescription.insertNewObjectForEntityForName("Records", inManagedObjectContext: context)
            let num:Int = Int(rollNumberTF.text!)!
            insertEntity.setValue(num, forKey: "rollNumber")
            insertEntity.setValue(nameTF.text! as String, forKey: "name")
            insertEntity.setValue(departmentTF.text! as String, forKey: "department")
            
            do {
                try context.save()
                
                let controller = UIAlertController(title: "", message: "Saved", preferredStyle: UIAlertControllerStyle.Alert)
                let okBtnAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (nil) in
                    self.rollNumberTF.text = ""
                    self.nameTF.text = ""
                    self.departmentTF.text = ""
                })
                
                controller.addAction(okBtnAction)
                self.presentViewController(controller, animated: true, completion: nil);
                
                //5
            } catch let error as NSError  {
                print("Could not save \(error), \(error.userInfo)")
            }
        }
    }
    
    func cancelBtnAction(sender:AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
