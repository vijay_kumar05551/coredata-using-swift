//
//  ViewController.swift
//  CoreData using swift
//
//  Created by OSX on 25/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var recordsTV: UITableView!
    var recordsArray = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = delegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "Records")
        
        do {
            let result = try context.executeFetchRequest(fetchRequest)
            recordsArray = result as! [NSManagedObject]
            recordsTV.reloadData()
        } catch {
            
        }
    }
    
    @IBAction func addRecordsAction(sender: AnyObject) {
        let controller = self.storyboard?.instantiateViewControllerWithIdentifier("AddRecordsViewController") as! AddRecordsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier:NSString = "recordsCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String) ?? UITableViewCell(style: .Default, reuseIdentifier: cellIdentifier as String)
        
        let objectModel = recordsArray[indexPath.row]
        cell.textLabel?.text = "\(objectModel.valueForKey("name") as! String)                           \(objectModel.valueForKey("department") as! String)"
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let controller = self.storyboard?.instantiateViewControllerWithIdentifier("AddRecordsViewController") as! AddRecordsViewController
        controller.manageObject = recordsArray
        controller.isUpdate = true
        controller.index = indexPath.row as Int
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch editingStyle {
        case .Delete:
            // remove the deleted item from the model
            let appDel:AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let context:NSManagedObjectContext = appDel.managedObjectContext
            context.deleteObject(recordsArray[indexPath.row] )
            recordsArray.removeAtIndex(indexPath.row)
            do {
                try context.save()
            } catch _ {
            }
            
            // remove the deleted item from the `UITableView`
            recordsTV.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
